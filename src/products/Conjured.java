package products;

import plugins.DecrementQuality;
import refactorizado.Item;

public class Conjured implements IProducto {

    @Override
    public void update(Item item) {
        DecrementQuality.run(item, 2);
    }
    
}
