package products;

import refactorizado.Item;

public interface IProducto {
    public void update(Item item);
}
