package products;

import plugins.IncrementQuality;
import refactorizado.Item;

public class Backstage implements IProducto {

    @Override
    public void update(Item item) {
        if (item.sellIn > 10) {
            IncrementQuality.run(item, 1);
        }
        if (item.sellIn > 5 && item.sellIn <= 10) {
            IncrementQuality.run(item, 2);
        }
        if (item.sellIn > 0 && item.sellIn <= 5) {
            IncrementQuality.run(item, 3);
        }
        if(item.sellIn == 0)
            item.quality = 0;
    }

}
