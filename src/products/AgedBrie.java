package products;

import plugins.IncrementQuality;
import refactorizado.Item;

public class AgedBrie implements IProducto {

    @Override
    public void update(Item item) {
        IncrementQuality.run(item, 1);
    }
    
}
