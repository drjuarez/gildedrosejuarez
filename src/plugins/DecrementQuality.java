package plugins;

import refactorizado.Item;

public class DecrementQuality {

    public static void run(Item item, int cantidad) {
        if (item.sellIn == 0 && (item.quality - cantidad) > 0) {
            item.quality -= cantidad;
        }
        if (item.sellIn <= 0) {
            if ((item.quality - 2) < 0) {
                item.quality = 0;
            } else {
                item.quality -= 2;
            }
        } else {
            if ((item.quality - cantidad) < 0) {
                item.quality = 0;
            } else {
                item.quality -= cantidad;
            }
        }
    }

}
