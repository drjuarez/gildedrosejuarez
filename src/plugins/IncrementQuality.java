package plugins;

import refactorizado.Item;

public class IncrementQuality {

    public static void run(Item item, int cantidad) {
        if ((item.quality + cantidad) > 50) {
            return;
        }
        item.quality += cantidad;
    }

}
