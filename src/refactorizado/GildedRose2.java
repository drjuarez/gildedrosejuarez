package refactorizado;

import products.AgedBrie;
import products.Backstage;
import products.Conjured;
import products.IProducto;
import products.Sulfura;

public class GildedRose2 {

    Item[] items;

    public GildedRose2(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            if (item.name.contains("Aged Brie")) {
                new AgedBrie().update(item);
            }else if (item.name.contains("Sulfuras, Hand of Ragnaros")) {
                new Sulfura().update(item);
            }else if (item.name.contains("Backstage passes to a TAFKAL80ETC concert")) {
                new Backstage().update(item);
            }else if (item.name.contains("Conjured Mana Cake")) {
                new Conjured().update(item);
            }else{
                decrementQuality(item);
            }
            this.decrementSellIn(item);
        }
    }

    public void incrementQuality(Item item) {
        if (item.quality >= 50) {
            return;
        }
        item.quality++;
    }

    public void decrementQuality(Item item) {
        if (item.sellIn <= 0) {
            if ((item.quality - 2) < 0) {
                item.quality = 0;
            } else {
                item.quality -= 2;
            }
        } else {
            if (item.quality>0) {
                item.quality--;
            }
        }
    }

    public void decrementSellIn(Item item) {
        if (item.sellIn > 0) {
            item.sellIn --;
        }
    }
}
